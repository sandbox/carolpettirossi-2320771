<?php
/**
 * CSV API common functions
 */

/**
 * Import a CSV file to respective entity
 */
function _csv_api_import_file($file_name = ''){
  //@TODO
  //pass $columns_to_read argument
  $errors = _csv_api_validate_csv($file_name, 2);
  $data_to_import = $errors;
  if (count($errors) == 0) {
    $data_to_import = _csv_api_parse_csv($file_name);
  }
  return $data_to_import;
}

/**
 * Export CSV File based on configuration
 * @param array $data used to generate the file
 * @param boolean $download TRUE to return download file FALSE to return the file object
 * @param string $file_name name to export file
 * Return: File to download
 */
function _csv_api_export_file($data, $download = TRUE, $file_name='export'){
  $file_name = $file_name . '_' . time() . '.csv';
  $delimiter = variable_get('csv_api_config_export_delimiter');
  $delimiter    = _csv_api_replace_special_chars($delimiter);
  $end_of_line = variable_get('csv_api_config_export_end_of_line');
  $end_of_line = _csv_api_replace_special_chars($end_of_line);
  //CSV Columns
  $file_content = implode($delimiter, array_keys($data[0]));
  foreach ($data as $row_key => $row_value) {
    $file_content .= $end_of_line . implode($delimiter, array_values($row_value));
  }

  $destination = 'private://csv_api/exports/';
  file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
  $destination .=  $file_name;
  // what to do when the destination file already exists
  $replace = FILE_EXISTS_REPLACE;
  $my_file_obj = file_save_data($file_content, $destination, $replace);


  if ($download) {
    $headers = file_get_content_headers($my_file_obj);
    $headers['Content-Disposition'] = "attachment; filename=" . $file_name;
    return file_transfer($my_file_obj->uri, $headers);
  }
  else {
    return $my_file_obj;
  }

}

/**
 * Parse data from file
 * @param string $file_name
 * @return array data from csv
 */
function _csv_api_parse_csv($file_path){
  $file_content = _csv_api_read_file($file_path);
  $end_of_line  = variable_get('csv_api_config_import_end_of_line');
  $end_of_line  = _csv_api_replace_special_chars($end_of_line);
  $rows         = str_getcsv($file_content, $end_of_line);
  $delimiter    = variable_get('csv_api_config_import_delimiter');
  $delimiter    = _csv_api_replace_special_chars($delimiter);
  $header_columns = str_getcsv($rows[0], $delimiter);
  $data = array();


  foreach ($rows as $row_key => $row_value) {
    //Ignore header row
    if($row_key > 0){
      $row_value = str_getcsv($row_value, $delimiter);
      foreach ($header_columns as $header_key => $header_value) {
        $data[$row_key-1][$header_value] = trim($row_value[$header_key]);
      }
    }
  }
  return $data;
}

/**
 * Validate CSV file
 * @param string $file_path - string containing the file_path
 * @param int $columns_to_read - number of columns to read
 * @return array errors - Array with validation errors
 */
function _csv_api_validate_csv($file_path, $columns_to_read=1){
  $errors = array();
  $file_content = _csv_api_read_file($file_path);

  if (empty($file_content)) {
    $errors[] = t('file is empty');
  } else {
    $delimiter   = variable_get('csv_api_config_import_delimiter');
    $delimiter   = _csv_api_replace_special_chars($delimiter);
    $end_of_line = variable_get('csv_api_config_import_end_of_line');
    $end_of_line = _csv_api_replace_special_chars($end_of_line);
    $file_content_end_of_line = str_getcsv ($file_content, $end_of_line);
    if (count($file_content_end_of_line) == 1) {
      $errors[] = t('file needs to has one header and at least one row');
    } else {
      $file_content_delimiter = str_getcsv($file_content_end_of_line[0], $delimiter);
      if (count($file_content_delimiter) < $columns_to_read) {
        $errors[] = t('File header column\'s quantity different than expected or the character delimiter different than expected.');
      } else {
        foreach ($file_content_end_of_line as $row_key=>$row_value) {
          if($row_key > 0){
            if(count(str_getcsv($row_value, $delimiter)) < $columns_to_read){
              $errors[] = t('file row ' . ($row_key + 1) . ' has invalid quantity of columns');
            }
          }
        }
      }
    }
  }

  return $errors;
}

/**
 * Parse EOL or columns delimiter to replace special chars like \n and \t
 * Database save these chars with double "\".
 * @param string $delimiter delimiter for EOL or Column
 * @return string EOL parsed
 */
function _csv_api_replace_special_chars($delimiter){
  $delimiter = str_replace("\\t", "\t", $delimiter);
  $delimiter = str_replace("\\n", "\n", $delimiter);
  $delimiter = str_replace("\\r", "\r", $delimiter);
  return $delimiter;
}

/**
 * Read file
 * @param string $file_name
 * @return file_content - string of file
 */
function _csv_api_read_file($file_path){
  if (!file_exists($file_path)) {
    return drupal_set_message(t('file not exists.'));
  }
  $file_content = file_get_contents_utf8($file_path);
  return $file_content;
}


/**
 * Gets the content of the given file and deal with utf-8 enconding
 * A workaround for file_get_contents according to:
 * http://php.net/manual/en/function.file-get-contents.php#85008
 * @param type $fn
 * @return type
 */
function file_get_contents_utf8($file_path) {
  $content = file_get_contents($file_path);
  return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
}

/**
 * get configured mappings
 * @param string $system_settings_name used to find the desired seetings
 * @return array $settings settings configuration
 */
function _csv_api_mappings_settings($system_settings_name='csv_api_mappings'){
  return unserialize(variable_get($system_settings_name));
}

/**
 * get configured mappings columns
 * @param string $system_settings_name used to find the desired seetings
 * @return array $columns csv columns mapped
 */
function _csv_api_mappings_keys_and_columns($system_settings_name='csv_api_mappings'){
  $settings = _csv_api_mappings_settings($system_settings_name);
  $columns = array();
  foreach ($settings as $key => $setting) {
    if($setting['enabled'] == '1'){
      $columns[$key] = $setting['value'];
    }
  }

  return $columns;
}
