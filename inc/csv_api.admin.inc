<?php

/**
 * Implements admin config form.
 * CSV API admin form.
 */
function csv_api_config_form($form, $form_state) {

  $form = array();

  //Import
  $form['csv_api_config_import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['csv_api_config_import']['csv_api_config_import_delimiter'] = array(
    '#type' => 'textfield',
    '#title' => t('Delimiter'),
    '#default_value' => variable_get('csv_api_config_import_delimiter', '|'),
    '#required' => TRUE,
    '#description' => t('Delimiter used to separate CSV columns. If you want to use TAB, please fill out the field with \'\t\'. default: |'),
  );

  $form['csv_api_config_import']['csv_api_config_import_end_of_line'] = array(
    '#type' => 'textfield',
    '#title' => t('End of Line'),
    '#default_value' => variable_get('csv_api_config_import_end_of_line', '\n'),
    '#required' => TRUE,
    '#description' => t('EOL used to separate CSV lines. default: \n'),
  );

  $form['csv_api_config_import']['csv_api_config_import_file_location'] = array(
    '#type' => 'textfield',
    '#title' => t('File save location'),
    '#default_value' => variable_get('csv_api_config_import_file_location', 'csv_api'),
    '#required' => TRUE,
    '#description' => t("Server's path to store imported files. default: csv_api"),
  );

  //Export
  $form['csv_api_config_export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['csv_api_config_export']['csv_api_config_export_delimiter'] = array(
    '#type' => 'textfield',
    '#title' => t('Delimiter'),
    '#default_value' => variable_get('csv_api_config_export_delimiter', '|'),
    '#required' => TRUE,
    '#description' => t('Delimiter used to separate CSV columns. If you want to use TAB, please fill out the field with \'\t\'. Default: |'),
  );

  $form['csv_api_config_export']['csv_api_config_export_end_of_line'] = array(
    '#type' => 'textfield',
    '#title' => t('End of Line'),
    '#default_value' => variable_get('csv_api_config_export_end_of_line', '\n'),
    '#required' => TRUE,
    '#description' => t('EOL used to separate CSV lines. default: \n'),
  );

  return system_settings_form($form);
}

/**
 * Implements Generic CSV X Fields mappings config form.
 * @param array $fields - fields used to map
 * @param string $system_settings_name - name used to save settings
 * @return drupal form
 */
function csv_api_mappings_config_form($fields, $system_settings_name="csv_api_mappings") {

  $form = array();

  // Build the sortable table header.
  $header = array(
    'field' => t('Field'),
    'csv_column' => t('CSV Column'),
    'enabled' => t('Enabled'),
  );

  //Build the rows.
  $options = array();
  
  $settings = _csv_api_mappings_settings($system_settings_name);
  
  foreach ($fields as $field_key) {
    $options[$system_settings_name . '_' . $field_key] = array(
        'field' => $field_key,
        'csv_column' => array(
            'data' =>  array(
                '#id' => $system_settings_name . '_' . $field_key,
                '#name' => $system_settings_name . '_' . $field_key,
                '#type' => 'textfield',
                '#value' => isset($settings[$field_key]['value'])? $settings[$field_key]['value'] : $field_key,
            ),
        ),
        'enabled' => array(
            'data' =>  array(
                '#id' => $system_settings_name . '_' . $field_key . '_enabled',
                '#name' => $system_settings_name . '_' . $field_key . '_enabled',
                '#type' => 'checkbox',
                '#attributes' => !isset($settings[$field_key]['enabled']) || $settings[$field_key]['enabled'] == '1' ? array('checked' => 'checked') : array(),
            ),
        ),
    );
  }

  $form[$system_settings_name] = array(
      '#tree' => TRUE,
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No mappings available.'),
      '#js_select' => TRUE,
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Implements Generic CSV X Fields mappings config form submit
 * @param array $fields - fields used to map
 * @param array $values - values used to save
 * @param string $system_settings_name - name used to save settings
 * @return drupal form
 */
function csv_api_mappings_config_form_submit($fields, $values, $system_settings_name="csv_api_mappings") {
  $settings = array();
  foreach ($fields as $field_key) {
    $settings[$field_key]['value'] = $values[$system_settings_name . '_' . $field_key];
    if(isset($values[$system_settings_name . '_' . $field_key . '_enabled'])){
      $settings[$field_key]['enabled'] = '1';
    } else {
      $settings[$field_key]['enabled'] = '0';
    }
  }
  variable_set($system_settings_name, serialize($settings));
  drupal_set_message(t('Configurations saved.'));
}
